package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC002_CreateLead extends SeleniumBase {
	@Test
	public void CreateLead() {
		
	startApp("chrome", "http://leaftaps.com/opentaps/");
	
    WebElement eleusername = locateElement("id","username");
    
    clearAndType(eleusername, "DemoSalesManager");
    WebElement elepassword = locateElement("id","password");
    clearAndType(elepassword, "crmsfa");
    
    WebElement elelogin = locateElement("class","decorativeSubmit");
    
    click(elelogin);
    
    WebElement elecrmslink = locateElement("LinkText","CRM/SFA");
    
    click(elecrmslink);
    
    WebElement eleLeads = locateElement("LinkText","Leads");
    
 click(eleLeads);
 
   locateElement("Xpath","href=\"/crmsfa/control/createLeadForm\"");
    
	}

}
