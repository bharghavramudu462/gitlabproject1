package week5day1;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	
	//class for creating html file
	ExtentHtmlReporter html;
	//to create a reports based on the testlog
	ExtentReports extent;
	//to put the testlogs in the Reports
	ExtentTest test;
	
	@Test
	
	public void runReport() 
	{
		
    html = new ExtentHtmlReporter("./report/extentReport.html");
    extent =  new ExtentReports();
    
   //to append the report for every run
    html.setAppendExisting(true);
	extent.attachReporter(html);
	//test name and description
	test = extent.createTest("TC001_Login","Login into Leaftaps");
	//Filter using username
	test.assignAuthor("Bhargav");
	test.assignCategory("Regression");
	
	try {
		
	test.pass("User login is successfull",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
	}
	
	catch (Exception e) {
		
		e.printStackTrace();
		
	}
extent.flush();
		
	}
	
	
	
	}
	
	
	


